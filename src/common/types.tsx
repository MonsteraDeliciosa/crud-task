export type ItemProps = {
  id: string;
  name: string;
  ingredients: string[];
  open: boolean;
  onTriggerClick: Function;
  onEditClick: Function;
  onDeleteClick: Function;
};

export type ItemState = {};

export type Item = {
  id: string;
  name: string;
  ingredients: string[];
  open: boolean;
  dateAdded: number;
};

export type ItemUpdate = {
  name: string;
  ingredients: string[];
};

export type ItemListState = {
  items: Item[];
  activeItem: string | null;
  modalOpen: boolean;
}

type backgroundVariant = "dark" | "light" | "warn" | "transparent";

export type ButtonProps = {
  text: string;
  onClick: Function;
  backgroundVariant ? : backgroundVariant;
};

export type ModalProps = {
  title: string;
  nameInputId: string;
  nameInputLabel: string;
  nameInputPlaceholder?: string;
  inputValue?: string;
  ingredientsInputId: string;
  ingredientsInputLabel: string;
  ingredientsInputPlaceholder?: string;
  textareaValue?: string;
  onSubmit: Function;
  submitBtnText: string;
  onModalClose: Function;
  closeBtnText: string;
  isNewItemModal?: boolean;
  itemId?: string;
};

export type ModalState = {
  nameInputValue: string;
  ingredientsTextareaValue: string;
};