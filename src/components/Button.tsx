import React from "react";
import "./Button.css";
import { ButtonProps } from './../common/types'

const StyledButton: React.FC<ButtonProps> = ({onClick, backgroundVariant = "dark", text}) => (
  <button
    className={`Button Button-${backgroundVariant}`}
    onClick={(...props) => onClick(props)}
  >
    {text}
  </button>
);

export default StyledButton;
