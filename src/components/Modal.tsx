import React, {Component} from "react";
import "./Modal.css";
import { ModalProps, ModalState, Item, ItemUpdate } from "./../common/types";
import StyledButton from './Button'

class StatefulModal extends Component<ModalProps, ModalState> {
  constructor(props: ModalProps) {
    super(props);
    this.props = props;
  }

  state: ModalState = {
    nameInputValue: "",
    ingredientsTextareaValue: "",
  };

  props: ModalProps = {
    title: "",
    nameInputId: "",
    nameInputLabel: "",
    ingredientsInputId: "",
    ingredientsInputLabel: "",
    onSubmit: () => {},
    submitBtnText: "",
    onModalClose: () => {},
    closeBtnText: "",
    isNewItemModal: false,
  };

  componentDidMount() {
    const { inputValue, textareaValue } = this.props;
    if (
      inputValue &&
      inputValue.length &&
      textareaValue &&
      textareaValue.length
    ) {
      this.setState({
        nameInputValue: inputValue,
        ingredientsTextareaValue: textareaValue,
      });
    }
  }

  private handleInputChange = (e: React.FormEvent<HTMLInputElement>) => {
    this.setState({ nameInputValue: e.currentTarget.value });
  };

  private handleTextareaChange = (e: React.FormEvent<HTMLTextAreaElement>) => {
    this.setState({ ingredientsTextareaValue: e.currentTarget.value });
  };

  private formatIngredients = (ingStr: string) => ingStr.split(',')

  private handleEdit = () => {
    const updatedProperties: ItemUpdate = {
      name: this.state.nameInputValue,
      ingredients: this.formatIngredients(this.state.ingredientsTextareaValue),
    };
    this.props.onSubmit(this.props.itemId, updatedProperties);
  }

  private handleSubmit = () => {
    const newItem: Item = {
      id: Math.floor(Math.random() * 100000).toString(),
      name: this.state.nameInputValue,
      ingredients: this.formatIngredients(this.state.ingredientsTextareaValue),
      open: false,
      dateAdded: Date.now(),
    };
    this.props.onSubmit(newItem);
  };

  private handleClose = () => {
    this.props.onModalClose();
  }

  public render() {
    return (
      <div className="Modal">
        <div className="Modal-overlay">
          <div className="Modal-content">
            <header className="Modal-header">
              <h3 className="Modal-title">{this.props.title}</h3>
              <StyledButton
                onClick={this.handleClose}
                text={"X"}
                backgroundVariant="transparent"
              />
            </header>
            <form className="Modal-form" onSubmit={(e) => e.preventDefault()}>
              <div className="Modal-formfields-group">
                <label
                  className="Modal-formfield"
                  htmlFor={this.props.nameInputId}
                >
                  {this.props.nameInputLabel}
                </label>
                <input
                  className="Modal-formfield"
                  id={this.props.nameInputId}
                  type="text"
                  placeholder={this.props.nameInputPlaceholder}
                  onChange={this.handleInputChange}
                  value={this.state.nameInputValue}
                />
              </div>
              <div className="Modal-formfields-group">
                <label
                  className="Modal-formfield"
                  htmlFor={this.props.ingredientsInputId}
                >
                  {this.props.ingredientsInputLabel}
                </label>
                <textarea
                  className="Modal-formfield"
                  id={this.props.ingredientsInputId}
                  placeholder={this.props.ingredientsInputPlaceholder}
                  onChange={this.handleTextareaChange}
                  value={this.state.ingredientsTextareaValue}
                />
              </div>
              <StyledButton
                onClick={this.props.isNewItemModal ? this.handleSubmit : this.handleEdit}
                text={this.props.submitBtnText}
                backgroundVariant="dark"
              />
              <StyledButton
                onClick={this.handleClose}
                text={this.props.closeBtnText}
                backgroundVariant="light"
              />
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default StatefulModal;
