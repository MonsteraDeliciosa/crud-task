import React from "react";
import { render } from "@testing-library/react";
import ListItem from "./ListItem";

describe('ListItem component:', () => {
    test("renders delete button", () => {
        const { getByText } = render(
          <ListItem
            open={true}
            id="test"
            name="testName"
            ingredients={["a", "b", "c"]}
            onTriggerClick={() => {}}
            onEditClick={() => {}}
            onDeleteClick={() => {}}
          />
        );
      const deleteBtn = getByText(/delete/i);
      expect(deleteBtn).toBeInTheDocument();
    });

    test("renders edit button", () => {
        const { getByText } = render(
          <ListItem
            open={true}
            id="test"
            name="testName"
            ingredients={["a", "b", "c"]}
            onTriggerClick={() => {}}
            onEditClick={() => {}}
            onDeleteClick={() => {}}
          />
        );
        const edit = getByText(/edit/i);
        expect(edit).toBeInTheDocument();
    });

    test("ingredients are visible when item is not active", () => {
      const { container } = render(
        <ListItem
          open={true}
          id="test"
          name="testName"
          ingredients={["a", "b", "c"]}
          onTriggerClick={() => {}}
          onEditClick={() => {}}
          onDeleteClick={() => {}}
        />
      );
      const ingredients = container.querySelector('[class="ListItem-accordion-ingredients-list"]');
      expect(ingredients).toBeVisible()
    });

    test("ingredients are not visible when item is not active", () => {
      const { container } = render(
        <ListItem
          open={false}
          id="test"
          name="testName"
          ingredients={["a", "b", "c"]}
          onTriggerClick={() => {}}
          onEditClick={() => {}}
          onDeleteClick={() => {}}
        />
      );
      const ingredients = container.querySelector('[class="ListItem-accordion-ingredients-list"]');
      expect(ingredients).not.toBeInTheDocument()
    });
})

