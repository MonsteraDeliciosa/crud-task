import React, {Component} from "react";
import { ItemListState, Item as ItemType, ItemUpdate } from "../common/types";
import "./ItemList.css";
import Item from './ListItem'
import Modal from "./Modal";
import StyledButton from "./Button";

class ItemList extends Component<{}, ItemListState> {
  state: ItemListState = {
    items: [],
    activeItem: null,
    modalOpen: false,
  };

  componentDidMount() {
    const savedItems = localStorage.getItem("items");
    if (savedItems) {
      this.setState({items: JSON.parse(savedItems)})
    }
  }

  private addNewItem = (item: ItemType) => {
    const updatedItems = [...this.state.items, item];
    this.setState({ items: updatedItems});
    localStorage.setItem("items", JSON.stringify(updatedItems))
    this.closeModal();
  };

  private sortByDate = (a: ItemType, b: ItemType) => a.dateAdded - b.dateAdded;

  private toggleActiveItemOpenState = (id: string) => {
    const newItems: ItemType[] = this.state.items.filter(
      (item) => item.id !== id
    );
    const closedItems: ItemType[] = [];
    newItems.forEach((item) => {
      closedItems.push({ ...item, open: false });
    });
    const updatedItem: ItemType = this.state.items.filter(
      (item) => item.id === id
    )[0];

    const updatedItems = [
      ...closedItems,
      { ...updatedItem, open: !updatedItem.open },
    ].sort(this.sortByDate);

    this.setState({
      items: updatedItems,
      activeItem: id,
    });
  };

  private openModal = () => {
    this.setState({ modalOpen: true });
  };

  private closeModal = () => {
    this.setState({ modalOpen: false });
  };

  private setActiveItem = (id: string) => {
    this.setState({ activeItem: id });
  };

  private unsetActiveItem = () => {
    this.setState({ activeItem: null });
  };

  private openNewItemModal = () => {
    this.unsetActiveItem();
    this.openModal();
  };

  private getActiveItem = (id: string) => {
    const activeItem: ItemType = this.state.items.filter((item) => item.id === id)[0];

    return activeItem;
  }

  private openEditModal = (id: string) => {
    this.setActiveItem(id);
    this.openModal();
  };

  private saveActiveItemChanges = (id: string, newProperties: ItemUpdate) => {
    const { name, ingredients } = newProperties;
    const oldItems = this.state.items.filter((item) => item.id !== id);
    const updatedItem = this.state.items.filter((item) => item.id === id)[0];
    const updatedItems = [...oldItems, { ...updatedItem, name, ingredients }].sort(this.sortByDate);

    this.setState({ items: updatedItems });
    localStorage.setItem("items", JSON.stringify(updatedItems));
    this.closeModal();
  };

  private deleteItem = (id: string) => {
    const updatedItems = this.state.items.filter((item) => item.id !== id);
    localStorage.setItem("items", JSON.stringify(updatedItems));
    this.setState({
      items: updatedItems,
      activeItem: null,
    });
  };

  private renderModal = () => {
    const { activeItem, modalOpen } = this.state;
    if (activeItem && modalOpen) {
      return (
        <Modal
          title={`Edit: ${this.getActiveItem(activeItem).name}`}
          nameInputId="nameInputId"
          nameInputLabel="Recipe name"
          inputValue={this.getActiveItem(activeItem).name}
          ingredientsInputId="ingredientsInputId"
          ingredientsInputLabel="Ingredients"
          textareaValue={this.getActiveItem(activeItem).ingredients.toString()}
          onSubmit={this.saveActiveItemChanges}
          submitBtnText="Save"
          onModalClose={this.closeModal}
          closeBtnText="Close"
          isNewItemModal={false}
          itemId={activeItem}
        />
      )
    }
    if (!activeItem && modalOpen) {
      return (
        <Modal
          title="Add recipe"
          nameInputId="nameInputId"
          nameInputLabel="Name"
          nameInputPlaceholder="e.g. Lasagna"
          ingredientsInputId="ingredientsInputId"
          ingredientsInputLabel="Ingredients"
          ingredientsInputPlaceholder="e.g. Pasta, tomatoes, parmesan"
          onSubmit={this.addNewItem}
          submitBtnText="Submit"
          onModalClose={this.closeModal}
          closeBtnText="Close"
          isNewItemModal={true}
        />
      )
    }
    return null;
  }

  private renderItems = (items: ItemType[]) => {
    if (!items) return null;
    return items.map((item: any) => {
      return (
        <Item
          id={item.id}
          name={item.name}
          ingredients={item.ingredients}
          key={item.name + (Math.random() * 20).toString()}
          open={item.open}
          onTriggerClick={this.toggleActiveItemOpenState}
          onEditClick={this.openEditModal}
          onDeleteClick={this.deleteItem}
        />
      );
    });
  };

  public render() {
    const { items } = this.state;
    return (
      <div className="ItemList">
        <h2 className="ItemList-header">Items:</h2>
        <ul className="ItemList-list">{this.renderItems(items)}</ul>
        <StyledButton
          onClick={() => this.openNewItemModal()}
          text="Add new item"
          backgroundVariant="light"
        />
        {this.renderModal()}
      </div>
    );
  }
}

export default ItemList