import React from "react";
import { render } from "@testing-library/react";
import ItemList from "./ItemList"

describe('Item List component:', () => {
  test("renders ItemList header", () => {
    const { getByText } = render(<ItemList />);
    const header = getByText(/items/i);
    expect(header).toBeInTheDocument();
  });

  test('initially has no items', () => {
    const {container} = render(<ItemList />)
    const listItem = container.querySelector("li");
    expect(listItem).toBeNull();
  })

  test('user can open new item modal', () => {
    const { getByText, container } = render(<ItemList />);
    const btn = getByText(/Add new item/i);
    expect(btn).toBeInTheDocument();
    btn.click();
    const modal = container.querySelector('[class="Modal"]');
    expect(modal).toBeInTheDocument();
  })
})