import React, { Component } from "react";
import { ItemProps, ItemState } from "../common/types";
import "./ListItem.css";
import StyledButton from './Button'

class ListItem extends Component<ItemProps, ItemState> {
  constructor(props: ItemProps) {
    super(props);
    this.props = props;
  }

  state: ItemState = {
    editTitleValue: this.props.name,
    editIngredientsValue: this.props.ingredients.toString(),
  };

  props: ItemProps = {
    id: "",
    name: "",
    ingredients: [],
    open: false,
    onTriggerClick: () => {},
    onEditClick: () => {},
    onDeleteClick: () => {},
  };

  private handleTriggerClick = () => {
    this.props.onTriggerClick(this.props.id);
  };

  private handleEditClick = (elementId: string) => {
    this.props.onEditClick(elementId);
  };
  private handleDeleteClick = (elementId: string) => {
    this.props.onDeleteClick(elementId);
  };

  public render() {
    const { id, name, ingredients, open } = this.props;
    return (
      <li className="ListItem">
        <button
          className="ListItem-accordion-trigger"
          onClick={this.handleTriggerClick}
        >
          {name}
        </button>
        {open && (
          <div className="ListItem-accordion-content">
            <ul className="ListItem-accordion-ingredients-list">
              {ingredients.map((el) => (
                <li
                  key={(Math.random() * 100).toString()}
                  className="ListItem-accordion-ingredients-item"
                >
                  {el}
                </li>
              ))}
            </ul>
            <StyledButton
              onClick={() => this.handleEditClick(id)}
              text="Edit"
            />
            <StyledButton
              onClick={() => this.handleDeleteClick(id)}
              text="Delete"
              backgroundVariant="warn"
            />
          </div>
        )}
      </li>
    );
  }
}

export default ListItem
