import React from "react";
import { render } from "@testing-library/react";
import Modal from "./Modal";

describe("Modal component:", () => {
    test("renders title", () => {
    const { getByText } = render(
        <Modal
        title="Modal title"
        nameInputId="nameInputId"
        nameInputLabel="Recipe"
        nameInputPlaceholder="placeholder"
        ingredientsInputId="ingredientsInputId"
        ingredientsInputLabel="Ingredients"
        ingredientsInputPlaceholder="placeholder"
        onSubmit={() => console.log("onSubmit")}
        submitBtnText="Submit"
        onModalClose={() => console.log("onModalClose")}
        closeBtnText="Close"
        />
    );
    const title = getByText(/Modal title/i);
    expect(title).toBeInTheDocument();
    });

    test("renders recipe name label and input", () => {
      const { getByText, getByPlaceholderText } = render(
        <Modal
          title="Modal title"
          nameInputId="nameInputId"
          nameInputLabel="Recipe"
          nameInputPlaceholder="nameInputPlaceholder"
          ingredientsInputId="ingredientsInputId"
          ingredientsInputLabel="Ingredients"
          ingredientsInputPlaceholder="placeholder"
          onSubmit={() => console.log("onSubmit")}
          submitBtnText="Submit"
          onModalClose={() => console.log("onModalClose")}
          closeBtnText="Close"
        />
      );
        const label = getByText(/Recipe/i);
        expect(label).toBeInTheDocument();
        const input = getByPlaceholderText(/nameInputPlaceholder/i);
        expect(input).toBeInTheDocument();
    });

    test("renders ingredients name label and input", () => {
        const { getByText, getByPlaceholderText } = render(
          <Modal
            title="Modal title"
            nameInputId="nameInputId"
            nameInputLabel="Recipe"
            nameInputPlaceholder="nameInputPlaceholder"
            ingredientsInputId="ingredientsInputId"
            ingredientsInputLabel="Ingredients"
            ingredientsInputPlaceholder="ingredientsInputPlaceholder"
            onSubmit={() => console.log("onSubmit")}
            submitBtnText="Submit"
            onModalClose={() => console.log("onModalClose")}
            closeBtnText="Close"
          />
        );
        const label = getByText(/Ingredients/i);
        expect(label).toBeInTheDocument();
        const input = getByPlaceholderText(/ingredientsInputPlaceholder/i);
        expect(input).toBeInTheDocument();
    });

    test("renders X close button", () => {
        const { getByText } = render(
        <Modal
            title="Modal title"
            nameInputId="nameInputId"
            nameInputLabel="Recipe"
            nameInputPlaceholder="placeholder"
            ingredientsInputId="ingredientsInputId"
            ingredientsInputLabel="Ingredients"
            ingredientsInputPlaceholder="placeholder"
            onSubmit={() => console.log("onSubmit")}
            submitBtnText="Submit"
            onModalClose={() => console.log("onModalClose")}
            closeBtnText="Close"
        />
        );
        const btn = getByText(/X/i);
        expect(btn).toBeInTheDocument();
    });

    test("renders named close button", () => {
        const { getByText } = render(
        <Modal
            title="Modal title"
            nameInputId="nameInputId"
            nameInputLabel="Recipe"
            nameInputPlaceholder="placeholder"
            ingredientsInputId="ingredientsInputId"
            ingredientsInputLabel="Ingredients"
            ingredientsInputPlaceholder="placeholder"
            onSubmit={() => console.log("onSubmit")}
            submitBtnText="Submit"
            onModalClose={() => console.log("onModalClose")}
            closeBtnText="Close"
        />
        );
        const btn = getByText(/Close/i);
        expect(btn).toBeInTheDocument();
    });

    test("renders submit button", () => {
        const { getByText } = render(
        <Modal
            title="Modal title"
            nameInputId="nameInputId"
            nameInputLabel="Recipe"
            nameInputPlaceholder="placeholder"
            ingredientsInputId="ingredientsInputId"
            ingredientsInputLabel="Ingredients"
            ingredientsInputPlaceholder="placeholder"
            onSubmit={() => console.log("onSubmit")}
            submitBtnText="Submit"
            onModalClose={() =>
            console.log("onModalClose")
            }
            closeBtnText="Close"
        />
        );
        const btn = getByText(/Submit/i);
        expect(btn).toBeInTheDocument();
    });
});
