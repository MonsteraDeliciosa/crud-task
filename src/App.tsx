import React from 'react';
import './App.css';
import ItemList from './components/ItemList'

const App = () => {
  return (
    <div className="App">
      <h1 className="App-header">Recipes list</h1>
      <ItemList />
    </div>
  );
}

export default App;
