# SETUP
App created using `create-react-app`.
## Installation
```
yarn
```
or
```
npm install
```

## Testing
Simple unit tests `testing-library`.
```
yarn test
```

# Ways to improve
- modal inputs validation
- more tests
- cool animations
- button for clearing local storage
- button for removing all recipes at once
- drag and drop for order change - currently recipes are sorted by date
- replacing css modules with CSS-in-JS approach


